#include <stdio.h>
#include <stdlib.h>

    typedef struct {
        char nome[50];
        float nota_prova1;
        float nota_prova2;
        int idade;
        int id;
    } FichaAluno;

void alocarMemoria();
void realocarMemoria();
void inserirAluno();
void exibirAluno();
void ordenacaoPorIdade();

int main() {
    
    /* Declara-se o ponteiro */
    FichaAluno *fa;
    int op, tam=0;

    fa = (FichaAluno *) malloc(tam * sizeof(FichaAluno));

    /* Verifica se conseguiu alocar memória. Equivale a if(c == NULL) */
    if (!fa) { printf("Não conseguiu alocar a memória\n"); exit(1); }

    do{
        printf("\n1 - Inserir alunos. ");
        printf("\n2 - Lista de todos alunos atualmente. ");
        printf("\n3 - Ordenacao por idade. "); 
        printf("\n4 - Sair do Programa. ");
        printf("\nInforme a opcao desejada: ");
        scanf("%d",&op);
        switch (op)
        {
            case 1:
                if(tam==0){
                    alocarMemoria(&fa, tam);
                    inserirAluno(fa, &tam);
                } else {
                    realocarMemoria(&fa, &tam);
                    inserirAluno(fa, &tam);
                }         
                break;
            case 2:
                if(tam==0){
					exibirAluno(&fa, tam);						
				} else {
			        exibirAluno(fa, tam);
				}
                break;
            case 3:
                OrdenacaoPorIdade(fa, tam);
                break;
            case 4:
                exit(1);
                break;    
            default:
                printf("A opcao informada e invalida");
                break;
        }   
    }while(1);
        
    /* libera-se a memória alocada */
    free(fa);
    return 0;

}

void alocarMemoria(FichaAluno **fa, int tam){
	*fa = (FichaAluno *) malloc((tam+1) * sizeof(FichaAluno));
	if (!*fa) { printf("Nao conseguiu alocar a memoria\n"); exit(1); }
}

void realocarMemoria(FichaAluno **fa, int *tam){
	*fa = (FichaAluno *) realloc(*fa, (*tam+1) * sizeof(FichaAluno));
	if (!*fa) { printf("\nNao conseguiu alocar a memoria\n"); exit(1); }
}

void inserirAluno(FichaAluno *fa, int *tam){

	    int pos = (*tam);

        fa[pos].id = (*tam)+1;
        printf("\nAluno numero %d ", fa[pos].id);
        
        printf("\nNome do Aluno: ");
        fflush(stdin);
        fgets(fa[pos].nome, 50, stdin);
        
        printf("Nota da primeira prova: ");
        scanf("%f", &fa[pos].nota_prova1);
        
        printf("Nota da segunda prova: ");
        scanf("%f", &fa[pos].nota_prova2); 
        
        printf("Digite a idade do aluno: ");
        scanf("%d", &fa[pos].idade);  

        (*tam)++;
    }

void exibirAluno(FichaAluno *fa, int tam){
    int pos = tam-1;

    for(pos=0 ; pos < tam; pos++){
            printf("\n_____________Aluno %d______________", pos+1);
            printf("\nNome do Aluno....................: %s", fa[pos].nome);
            printf("Nota da Primeira Prova...........: %.2f", fa[pos].nota_prova1);
            printf("\nNota da Segunda Prova............: %.2f", fa[pos].nota_prova2);
            printf("\nIdade do Aluno...................: %i", fa[pos].idade);
            printf("\n");
        }    
}        

void OrdenacaoPorIdade(FichaAluno *fa, int tam){
	int condParada=1;
	int i=0;

	while (condParada){
		condParada = 0;

		while (i < tam - 1){  
			if (fa[i].idade > fa[i+1].idade){

                FichaAluno aux;
                aux = fa[i];
				fa[i] = fa[i+1];
				fa[i+1] = aux;
				condParada = 1; 		
			}	
            i++;
		}
        i = 0;
	}
    printf("\n\n");
    printf("-------------Alunos Ordenados por Idade------------");
    printf("\n\n");

}